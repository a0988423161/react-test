import React from 'react';
// import logo from './logo.svg';
// import { Button } from '@material-ui/core';
// import { Counter } from './features/counter/Counter';
import './App.css';
import Dashboard from './page/Dashboard';

function App() {
  return (
    <div className="App">
      <Dashboard />
    </div>
  );
}

export default App;
