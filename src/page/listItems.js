import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
// import BarChartIcon from '@material-ui/icons/BarChart';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import TextsmsIcon from '@material-ui/icons/Textsms';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import PersonIcon from '@material-ui/icons/Person';
import AllInboxIcon from '@material-ui/icons/AllInbox';
// import PollIcon from '@material-ui/icons/Poll';
import BuildIcon from '@material-ui/icons/Build';
// import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
// import StarBorder from '@material-ui/icons/StarBorder';
import ViewListSharpIcon from '@material-ui/icons/ViewListSharp';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(2),
  },
  collapse:{
    // background:'#f5f5f5',
    fontSize:'12px'
  }
}));

export function MainListItems() {
  const classes = useStyles();
  const [open, setOpen] = useState([false,false,false]);
  const handleClick = (val) => {
    let newOpen = open.slice();
    newOpen[val] = !newOpen[val];
    setOpen(newOpen);
  }
  return(
    <div>
    <ListItem button onClick={() => handleClick(0)}>
      <ListItemIcon>
          <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="生產執行" />
      {open[0] ? <ExpandLess /> : <ExpandMore />}
    </ListItem>
    <Collapse in={open[0]} timeout="auto" unmountOnExit className={classes.collapse}>
      <List component="div" disablePadding>
        <ListItem button className={classes.nested}>
          <ListItemIcon>
            <ListAltIcon />
          </ListItemIcon>
          <ListItemText primary="工單管理" />
        </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <ImportExportIcon />
            </ListItemIcon>
            <ListItemText primary="進出站管理" />
          </ListItem>
      </List>
    </Collapse>
      <ListItem button onClick={() => handleClick(1)}>
        <ListItemIcon>
          <ViewListSharpIcon />
        </ListItemIcon>
        <ListItemText primary="生產管理" />
        {open[1] ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open[1]} timeout="auto" unmountOnExit className={classes.collapse}>
        <List component="div" disablePadding>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <AssignmentTurnedInIcon />
            </ListItemIcon>
            <ListItemText primary="產品管理" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary="產品控制計畫" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <ErrorIcon />
            </ListItemIcon>
            <ListItemText primary="不良品管理" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
          <AllInboxIcon />
            </ListItemIcon>
            <ListItemText primary="載具(盛裝工具)管理" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <PersonIcon />  
            </ListItemIcon>
            <ListItemText primary="QC人員管理" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <VpnKeyIcon />
            </ListItemIcon>
            <ListItemText primary="權限管理" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <TextsmsIcon />
            </ListItemIcon>
            <ListItemText primary="推播通知" />
          </ListItem>
        </List>
      </Collapse>
      <ListItem button>
        <ListItemIcon>
          <BuildIcon />
        </ListItemIcon>
        <ListItemText primary="設定" />
        {/* {open[2] ? <ExpandLess /> : <ExpandMore />} */}
      </ListItem>
    </div>
  )
}

export function SecondaryListItems() {
  return(
    <div>
      <ListSubheader inset>Saved reports</ListSubheader>
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Current month" />
      </ListItem>
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Last quarter" />
      </ListItem>
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="Year-end sale" />
      </ListItem>
    </div>
  )
};
