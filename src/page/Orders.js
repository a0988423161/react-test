import React from 'react';
// import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
// import Title from './Title';

// Generate Order Data
function createData( id, workpID, orderID, orderNo, CusNo, CusName, item, processOrder, comNo, workSNo, workPNum, divideNum, stationNum, stateName, date) {
  return { id, workpID, orderID, orderNo, CusNo, CusName, item, processOrder, comNo, workSNo, workPNum, divideNum, stationNum, stateName, date };
}

const rows = [
  createData(0, '531TD-191100053', 'P01VC-190300005', 1, 'A0-022','BWC','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'原料購入','2019/11/30 17:38:10'),
  createData(1, '531TD-191100053', 'P01VC-190300005', 1, 'B0-018','BWA','MAN-A1811036','0020','DSVC-003','DSO-03',10,100,100,'修蠟','2019/11/30 17:38:10'),
  createData(2, '531TD-191100053', 'P01VC-190300005', 2, 'A0-030','KKK','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'組樹','2019/11/30 17:38:10'),
  createData(3, '531TD-191100053', 'P01VC-190300005', 3, 'A0-008','MLW','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'原料購入','2019/11/30 17:38:10'),
  createData(4, '531TD-191100053', 'P01VC-190300005', 4, 'A0-022','Louis','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'原料購入','2019/11/30 17:38:10'),
  createData(5, '531TD-191100053', 'P01VC-190300005', 4, 'B0-009','LI','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'原料購入','2019/11/30 17:38:10'),
  createData(6, '531TD-191100053', 'P01VC-190300005', 5, 'A0-003','SAM','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'清洗','2019/11/30 17:38:10'),
  createData(7, '531TD-191100053', 'P01VC-190300005', 6, 'A0-032','TOM','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'鑄造','2019/11/30 17:38:10'),
  // createData(8, '531TD-191100053', 'P01VC-190300005', 7, 'B0-001','Cambrigde','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'原料購入','2019/11/30 17:38:10'),
  // createData(9, '531TD-191100053', 'P01VC-190300005', 7, 'A0-023','DDT','MAN-A1811036','0010','DSVC-003','DSO-03',10,100,100,'原料購入','2019/11/30 17:38:10'),
];

// function preventDefault(event) {
//   event.preventDefault();
// }

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Orders() {
  const classes = useStyles();
  return (
    <React.Fragment>
      {/* <Title>Recent Orders</Title> */}
      <Table size="simple">
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell>工單號</TableCell>
            <TableCell>訂單號</TableCell>
            <TableCell>訂單序號</TableCell>
            <TableCell>客戶代號</TableCell>
            <TableCell>客戶簡稱</TableCell>
            <TableCell>品名</TableCell>
            <TableCell>加工順序</TableCell>
            <TableCell>線別/廠商代號</TableCell>
            <TableCell>工段號</TableCell>
            <TableCell>拆分數量</TableCell>
            <TableCell>工單數量</TableCell>
            <TableCell>進站數量</TableCell>
            <TableCell>當前工作站</TableCell>
            <TableCell>進站日期</TableCell>
            {/* <TableCell align="right">tool</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell>{row.workpID}</TableCell>
            <TableCell>{row.orderID}</TableCell>
            <TableCell>{row.orderNo}</TableCell>
            <TableCell>{row.CusNo}</TableCell>
            <TableCell>{row.CusName}</TableCell>
            <TableCell>{row.item}</TableCell>
            <TableCell>{row.processOrder}</TableCell>
            <TableCell>{row.comNo}</TableCell>
            <TableCell>{row.workSNo}</TableCell>
            <TableCell>{row.workPNum}</TableCell>
            <TableCell>{row.divideNum}</TableCell>
            <TableCell>{row.stationNum}</TableCell>
            <TableCell>{row.stateName}</TableCell>
            <TableCell>{row.date}</TableCell>
              {/* <TableCell align="right">{row.tool}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <FiberManualRecordIcon />
        {/* <Link color="primary" href="#" onClick={preventDefault}>
          See more orders
        </Link> */}
      </div>
    </React.Fragment>
  );
}